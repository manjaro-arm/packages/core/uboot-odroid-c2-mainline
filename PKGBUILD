# U-Boot: ODROID-C2
# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Kevin Mihelich <kevin@archlinuxarm.org>

pkgname=uboot-odroid-c2-mainline
pkgver=2021.10
pkgrel=1
pkgdesc="Mainline U-Boot for ODROID-C2"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
install=$pkgname.install
makedepends=('git' 'bc' 'meson-tools')
provides=('uboot')
conflicts=('uboot')
_commit=205c7b3259559283161703a1a200b787c2c445a5
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://github.com/hardkernel/u-boot/archive/${_commit}.tar.gz")
md5sums=('f1392080facf59dd2c34096a5fd95d4c'
         '11a49bb7e9825b05fb555ba6f2aca736')

build() {
  cd u-boot-${pkgver/rc/-rc}

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  make distclean
  make odroid-c2_defconfig
  echo 'CONFIG_IDENT_STRING=" Manjaro ARM"' >> .config
  make -j1 EXTRAVERSION=-${pkgrel}

  cd ../u-boot-${_commit}
  make -C tools/fip_create
}

package() {
  install -d "${pkgdir}"/boot/extlinux

  u-boot-${_commit}/tools/fip_create/fip_create \
    --bl30  u-boot-${_commit}/fip/gxb/bl30.bin \
    --bl301 u-boot-${_commit}/fip/gxb/bl301.bin \
    --bl31  u-boot-${_commit}/fip/gxb/bl31.bin \
    --bl33  u-boot-${pkgver/rc/-rc}/u-boot.bin \
    fip.bin
  cat u-boot-${_commit}/fip/gxb/bl2.package fip.bin > boot_new.bin
  amlbootsig boot_new.bin u-boot.img
  dd if=u-boot.img of=u-boot.gxbb bs=512 skip=96

  cp u-boot-${_commit}/sd_fuse/bl1.bin.hardkernel u-boot.gxbb "${pkgdir}"/boot
}
